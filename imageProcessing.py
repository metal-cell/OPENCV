import os
import cv2
import collections
import numpy as np
import interface
import fileOperations
import globalVariables


def resizePictures(self, event):  # 修改镶元大小
    try:
        if globalVariables.sourceReadPath is None:
            return
        globalVariables.processing = True
        if not os.path.exists(globalVariables.readPath):
            os.makedirs(globalVariables.readPath)
            # 用一个列表保存所有的图片的文件名字
        files = os.listdir(globalVariables.sourceReadPath)
        # 清空之前的数据
        fileOperations.del_file(globalVariables.readPath)
        # n变量用来看到图片的处理进度。
        n = 0
        # 遍历所有图片文件们
        for file in files:
            n += 1
            # 控制进度条显示
            interface.Gauge(self, n, len(files))
            imgPath = globalVariables.sourceReadPath + "\\" + file  # 构造图片路径
            img = fileOperations.myImRead(imgPath)
            img = cv2.resize(img, (globalVariables.resizeX, globalVariables.resizeY))  # 更改图片的大小
            # 更改之后写入文件，方便以后使用。
            # cv2.imwrite(globalVariables.savePath + "\\" + file, img)
            fileOperations.myImWrite(globalVariables.savePath + "\\" + file, img)
        globalVariables.processing = False
        interface.INFORMATION(self, "修改镶元大小")
    except Exception as e:
        interface.Error(self, str(e))
        globalVariables.processing = False


def resizeBigPicture(self):
    try:
        if globalVariables.bigPicturePath is None:
            return
        globalVariables.processing = True
        img = fileOperations.myImRead(globalVariables.bigPicturePath)
        img = cv2.resize(img, (globalVariables.pictureX, globalVariables.pictureY))  # 更改图片的大小
        cv2.imwrite("Source.jpg", img)
        globalVariables.processing = False
    except Exception as e:
        interface.Error(self, str(e))
        globalVariables.processing = False


def pictureIndex(self, event):  # 生成图片索引
    try:
        globalVariables.processing = True
        files = os.listdir(globalVariables.readPath)
        n = 0
        s = ''
        for file in files:
            li = []
            n += 1
            # 控制进度条显示
            interface.Gauge(self, n, len(files))
            imgPath = globalVariables.readPath + "\\" + file
            img = fileOperations.myImRead(imgPath)
            print(str(n) + "/" + str(len(files)))
            if globalVariables.pictureIndexMode is True:  # pictureIndexMode = True  # 设置镶元索引模式 True为众数, False为平均值
                for i in range(globalVariables.resizeY):
                    for j in range(globalVariables.resizeX):
                        b = img[i, j, 0]
                        g = img[i, j, 1]
                        r = img[i, j, 2]
                        li.append((b, g, r))
                index = collections.Counter(li).most_common(1)
                s += file
                s += ":"
                s += str(index[0][0]).replace("(", "").replace(")", "")
                s += "\n"
            else:
                for i in range(globalVariables.resizeY):
                    for j in range(globalVariables.resizeX):
                        b = img[i, j, 0]
                        g = img[i, j, 1]
                        r = img[i, j, 2]
                        li.append((b, g, r))
                s += file
                s += ":"
                s += (str(int(sum([i[0] for i in li]) / (globalVariables.resizeX * globalVariables.resizeY))) + "," +
                      str(int(sum([i[1] for i in li]) / (globalVariables.resizeX * globalVariables.resizeY))) + "," +
                      str(int(sum([i[2] for i in li]) / (globalVariables.resizeX * globalVariables.resizeY))))
                s += "\n"
        f = open('pictureIndex.txt', 'w')
        f.write(s)
        globalVariables.processing = False
        interface.INFORMATION(self, "生成图片索引")
    except Exception as e:
        interface.Error(self, str(e))
        globalVariables.processing = False


# 获取列表的第二个元素
def takeSecond(elem):
    return elem[1]


def readIndex(self):  # 读取索引文件
    try:
        fs = open("pictureIndex.txt", "r")
        n = 0
        dic = []
        for line in fs.readlines():
            n += 1
            temp = line.split(":")
            file = temp[0]
            bgr = temp[1].split(",")
            b = int(bgr[0])
            g = int(bgr[1])
            r = int(bgr[2])
            dic.append((file, (b, g, r)))
        return dic
    except Exception as e:
        interface.Error(self, str(e))
        globalVariables.processing = False


def drawPicture(self, event):  # 绘制图像
    try:
        globalVariables.processing = True

        path = fileOperations.SaveFile(self, event)
        if path is None:
            globalVariables.processing = False
            return

        img = cv2.imread("Source.jpg")  # 用作大图模板的图
        s = np.shape(img)  # 读取图像(二维数组)的行数和列数
        big = np.zeros((globalVariables.resizeY * s[0], globalVariables.resizeX * s[1], 3), dtype=np.uint8)  # 新建大图
        fileList = readIndex(self)  # 读取索引文件到变量中, 类型为: (file, (b, g, r))
        n = 0
        for i in range(s[0]):  # 遍历行
            for j in range(s[1]):  # 遍历列
                b = img[i, j, 0]
                g = img[i, j, 1]
                r = img[i, j, 2]  # 获取图像当前位置的BGR值
                np.random.shuffle(fileList)  # 打乱索引文件, 避免生成的图片过于聚集
                filePath = ""  # 用作马赛克像素的图片路径
                distance = []  # 欧氏距离
                for index in range(len(fileList)):
                    item = fileList[index]
                    imgB = item[1][0]
                    imgG = item[1][1]
                    imgR = item[1][2]  # 获取索引文件的RGB值
                    distance.append((index, (imgB - b) ** 2 + (imgG - g) ** 2 + (imgR - r) ** 2))  # 欧式距离
                    print("distance: " + str(distance[index][0]) + ", " + str(distance[index][1]))
                    # 当欧式距离阈值时, 认定两种颜色相近
                    if globalVariables.drawPictureMode is False and distance[index][1] < globalVariables.thresholdValue:
                        filePath = globalVariables.readPath + "\\" + str(item[0])  # 定位到具体的图片文件
                        break
                if filePath == "":  # 如果找不到相似的图片, 则选取distance值最小的图片做像素点
                    distance.sort(key=takeSecond)  # 将二维数组distance按欧氏距离(第二个数据)从小到大排序
                    item = fileList[distance[0][0]]  # print("选取:" + str(distance[0][0]) + "号图片")
                    filePath = globalVariables.readPath + "\\" + str(item[0])  # 定位到具体的图片文件
                little = fileOperations.myImRead(filePath)
                big[i * globalVariables.resizeY:(i + 1) * globalVariables.resizeY,
                    j * globalVariables.resizeX:(j + 1) * globalVariables.resizeX] = little  # 把图片画到大图的相应位置
                n += 1
                interface.Gauge(self, n, s[0] * s[1])
        fileOperations.myImWrite(path, big)
        globalVariables.imgBig = big
        globalVariables.processing = False
        interface.INFORMATION(self, "生成图片")
    except Exception as e:
        interface.Error(self, str(e))
        globalVariables.processing = False


def colorModifier(self, event):
    try:
        globalVariables.processing = True

        if globalVariables.imgBig is None:
            globalVariables.processing = False
            return

        path = fileOperations.SaveFile(self, event)
        if path is None:
            globalVariables.processing = False
            return

        img = fileOperations.myImRead(globalVariables.bigPicturePath)
        s = np.shape(globalVariables.imgBig)  # 读取图像(二维数组)的行数和列数
        img = cv2.resize(img, (s[1], s[0]))
        print(img.shape)
        print(globalVariables.imgBig.shape)
        dst = cv2.addWeighted(img, globalVariables.percentage / 100,
                              globalVariables.imgBig, 1 - globalVariables.percentage / 100, 0)
        fileOperations.myImWrite(path, dst)
        globalVariables.processing = False
        interface.INFORMATION(self, "颜色修改")
    except Exception as e:
        interface.Error(self, str(e))
        globalVariables.processing = False
