# 存储全局变量
resizeX = 64   # 镶元宽
resizeY = 64   # 镶元高
pictureX = 160  # 马赛克原图宽
pictureY = 90  # 马赛克原图高
thresholdValue = 500  # 判断两个颜色是否相似的阈值
percentage = 35  # 颜色调整百分比
pictureIndexMode = True  # 设置镶元索引模式 True为众数, False为平均值
drawPictureMode = True  # 绘制图片模式 True为最接近, False为取阈值
sourceReadPath = None  # 这里是镶元图片集合所在的文件夹
bigPicturePath = None  # 马赛克原图路径
# 这里是改变大小之后的镶元图片，要保存的路径。save是一个文件夹
savePath = "resizePictures"
readPath = "resizePictures"
imgBig = None   # 生成的大图文件

processing = False