import os
import threading
import wx
import globalVariables
import imageProcessing
import fileOperations


class MyFrame(wx.Frame):
    def __init__(self):
        super().__init__(None, title="OpenCV马赛克拼贴", size=(500, 430))
        panel = wx.Panel(parent=self)

        title = wx.StaticText(parent=panel, label="OpenCV马赛克拼贴")
        font = wx.Font(18, wx.DECORATIVE, wx.NORMAL, wx.NORMAL)
        title.SetFont(font)

        self.text1 = wx.StaticText(parent=panel, label="1.选择镶元图片文件夹: ")
        b1 = wx.Button(parent=panel, id=10, label='选择文件夹')

        text2 = wx.StaticText(parent=panel, label="2.修改镶元大小: resizePictures")
        text2_1 = wx.StaticText(parent=panel, label="宽")
        self.tc2_1 = wx.TextCtrl(panel, size=(36, 24))
        text2_2 = wx.StaticText(parent=panel, label="高")
        self.tc2_2 = wx.TextCtrl(panel, size=(36, 24))
        self.b2 = wx.Button(parent=panel, id=20, label='修改镶元大小')

        text3 = wx.StaticText(parent=panel, label="3.生成镶元索引: pictureIndex.txt")
        radio3_1 = wx.RadioButton(panel, id=30, label='颜色众数', style=wx.RB_GROUP)
        radio3_2 = wx.RadioButton(panel, id=31, label='颜色平均值')
        self.b3 = wx.Button(parent=panel, id=30, label='生成镶元索引')

        self.text4 = wx.StaticText(parent=panel, label="4.设置马赛克原图: ")
        self.b4 = wx.Button(parent=panel, id=40, label='选择图片')

        text5 = wx.StaticText(parent=panel, label="5.调整马赛克原图大小: ")
        text5_1 = wx.StaticText(parent=panel, label="宽")
        self.tc5_1 = wx.TextCtrl(panel, size=(36, 24))
        text5_2 = wx.StaticText(parent=panel, label="高")
        self.tc5_2 = wx.TextCtrl(panel, size=(36, 24))

        text6 = wx.StaticText(parent=panel, label="6.绘制图片模式: ")
        radio6_1 = wx.RadioButton(panel, id=60, label='最接近', style=wx.RB_GROUP)
        radio6_2 = wx.RadioButton(panel, id=61, label='取阈值')
        text6_1 = wx.StaticText(parent=panel, label="阈值: ")
        self.tc6_1 = wx.TextCtrl(panel, size=(36, 24))

        self.b = wx.Button(parent=panel, id=0, label='生成图片!')

        text7 = wx.StaticText(parent=panel, label="7.颜色修改: ")
        text7_1 = wx.StaticText(parent=panel, label="百分比")
        self.tc7_1 = wx.TextCtrl(panel, size=(36, 24))
        text7_2 = wx.StaticText(parent=panel, label="%")
        self.b7 = wx.Button(parent=panel, id=70, label='颜色修改')
        self.b7.Enable(False)

        self.prompt = wx.StaticText(parent=panel, label="生成图片大小: (64*160)*(64*90)=10240*5760(像素)")

        # 进度条展示
        self.bEnd = wx.Button(parent=panel, id=100, label='使用帮助')
        self.gauge = wx.Gauge(parent=panel)
        self.gauge.SetValue(0)
        self.textEnd = wx.StaticText(parent=panel, label="0/0")

        vBox = wx.BoxSizer(wx.VERTICAL)

        vBox.Add(title, proportion=1, flag=wx.ALIGN_CENTER_HORIZONTAL)

        hBox1 = wx.BoxSizer(wx.HORIZONTAL)
        hBox1.Add(self.text1, proportion=3, flag=wx.EXPAND | wx.ALL, border=10)
        hBox1.Add(b1, proportion=1, flag=wx.EXPAND | wx.RIGHT, border=20)
        vBox.Add(hBox1, proportion=1, flag=wx.EXPAND | wx.TOP | wx.BOTTOM, border=2)

        hBox2 = wx.BoxSizer(wx.HORIZONTAL)
        hBox2.Add(text2, proportion=3, flag=wx.EXPAND | wx.ALL, border=10)
        hBox2.Add(text2_1, flag=wx.LEFT, border=10)
        hBox2.Add(self.tc2_1)
        hBox2.Add(text2_2, flag=wx.LEFT, border=10)
        hBox2.Add(self.tc2_2, flag=wx.RIGHT, border=10)
        hBox2.Add(self.b2, proportion=1, flag=wx.EXPAND | wx.RIGHT, border=20)
        vBox.Add(hBox2, proportion=1, flag=wx.EXPAND | wx.TOP | wx.BOTTOM, border=2)

        hBox3 = wx.BoxSizer(wx.HORIZONTAL)
        hBox3.Add(text3, proportion=3, flag=wx.EXPAND | wx.ALL, border=10)
        hBox3.Add(radio3_1, flag=wx.LEFT, border=10)
        hBox3.Add(radio3_2, flag=wx.RIGHT, border=20)
        hBox3.Add(self.b3, proportion=1, flag=wx.EXPAND | wx.RIGHT, border=20)
        vBox.Add(hBox3, proportion=1, flag=wx.EXPAND | wx.TOP | wx.BOTTOM, border=2)

        hBox4 = wx.BoxSizer(wx.HORIZONTAL)
        hBox4.Add(self.text4, proportion=3, flag=wx.EXPAND | wx.ALL, border=10)
        hBox4.Add(self.b4, proportion=1, flag=wx.EXPAND | wx.RIGHT, border=20)
        vBox.Add(hBox4, proportion=1, flag=wx.EXPAND | wx.TOP | wx.BOTTOM, border=2)

        hBox5 = wx.BoxSizer(wx.HORIZONTAL)
        hBox5.Add(text5, proportion=3, flag=wx.EXPAND | wx.ALL, border=10)
        hBox5.Add(text5_1)
        hBox5.Add(self.tc5_1, flag=wx.RIGHT, border=10)
        hBox5.Add(text5_2)
        hBox5.Add(self.tc5_2, flag=wx.RIGHT, border=20)
        vBox.Add(hBox5, proportion=1, flag=wx.EXPAND | wx.TOP | wx.BOTTOM, border=2)

        hBox6 = wx.BoxSizer(wx.HORIZONTAL)
        hBox6.Add(text6, proportion=3, flag=wx.EXPAND | wx.ALL, border=10)
        hBox6.Add(radio6_1)
        hBox6.Add(radio6_2)
        hBox6.Add(text6_1)
        hBox6.Add(self.tc6_1, flag=wx.RIGHT, border=20)
        vBox.Add(hBox6, proportion=1, flag=wx.EXPAND | wx.TOP | wx.BOTTOM, border=2)

        vBox.Add(self.b, proportion=1, flag=wx.EXPAND | wx.LEFT | wx.RIGHT, border=160)

        hBox7 = wx.BoxSizer(wx.HORIZONTAL)
        hBox7.Add(text7, proportion=3, flag=wx.EXPAND | wx.ALL, border=10)
        hBox7.Add(text7_1)
        hBox7.Add(self.tc7_1)
        hBox7.Add(text7_2, flag=wx.RIGHT, border=10)
        hBox7.Add(self.b7, proportion=1, flag=wx.EXPAND | wx.RIGHT, border=20)
        vBox.Add(hBox7, proportion=1, flag=wx.EXPAND | wx.TOP | wx.BOTTOM, border=2)

        vBox.Add(self.prompt, proportion=1, flag=wx.ALIGN_CENTER_HORIZONTAL | wx.TOP | wx.BOTTOM, border=5)

        hBoxEnd = wx.BoxSizer(wx.HORIZONTAL)
        hBoxEnd.Add(self.bEnd, proportion=15, flag=wx.LEFT, border=10)
        hBoxEnd.Add(self.gauge, proportion=65, flag=wx.LEFT, border=10)
        hBoxEnd.Add(self.textEnd, proportion=20, flag=wx.RIGHT, border=10)
        vBox.Add(hBoxEnd, proportion=1, flag=wx.EXPAND | wx.BOTTOM, border=2)

        panel.SetSizer(vBox)

        self.tc2_1.SetValue(str(globalVariables.resizeX))
        self.tc2_2.SetValue(str(globalVariables.resizeY))
        self.tc5_1.SetValue(str(globalVariables.pictureX))
        self.tc5_2.SetValue(str(globalVariables.pictureY))
        self.tc6_1.SetValue(str(globalVariables.thresholdValue))
        self.tc7_1.SetValue(str(globalVariables.percentage))

        self.Bind(wx.EVT_BUTTON, self.on_click)
        self.Bind(wx.EVT_TEXT, self.OnEnter)
        self.Bind(wx.EVT_RADIOBUTTON, self.on_radio_click)

    def on_click(self, event):
        event_id = event.GetId()  # 获得绑定按钮的id
        print(event_id)
        if globalVariables.processing is True and event_id != 100:
            return
        if event_id == 10:  # 选择镶元图片文件夹
            print("选择镶元图片文件夹")
            globalVariables.sourceReadPath = fileOperations.SelectFolder(self, event)
            if globalVariables.sourceReadPath is None:
                self.text1.SetLabelText("1.选择镶元图片文件夹: ")
            else:
                files = os.listdir(globalVariables.sourceReadPath)
                self.text1.SetLabelText("1.选择镶元图片文件夹: " + globalVariables.sourceReadPath + " (" + str(len(files))
                                        + "张图片)")
        if event_id == 20:  # 修改镶元图片大小
            print("修改镶元图片大小")
            #imageProcessing.resizePictures(self, event)
            thread = threading.Thread(target=imageProcessing.resizePictures, args=(self, event))
            # 启动线程
            thread.start()
        if event_id == 30:  # 生成镶元索引
            print("生成镶元索引")
            # imageProcessing.pictureIndex(self)
            thread = threading.Thread(target=imageProcessing.pictureIndex, args=(self, event))
            # 启动线程
            thread.start()
        if event_id == 40:  # 设置马赛克原图
            print("设置马赛克原图")
            globalVariables.bigPicturePath = fileOperations.OpenSingleFile(self, event)
            print(globalVariables.bigPicturePath)
            if globalVariables.bigPicturePath is None:
                self.text4.SetLabelText("4.设置马赛克原图: ")
            else:
                self.text4.SetLabelText("4.设置马赛克原图: " + globalVariables.bigPicturePath)
        if event_id == 0:  # 生成图片
            print("生成图片")
            imageProcessing.resizeBigPicture(self)
            # imageProcessing.drawPicture(self, event)
            thread = threading.Thread(target=imageProcessing.drawPicture, args=(self, event))
            # 启动线程
            thread.start()
            self.b7.Enable(True)
        if event_id == 70:  # 颜色修改
            print("颜色修改")
            # imageProcessing.colorModifier(self, event)
            thread = threading.Thread(target=imageProcessing.colorModifier, args=(self, event))
            # 启动线程
            thread.start()
        if event_id == 100:  # 使用帮助
            print("使用帮助")
            dlg = wx.MessageDialog(None, (
                    "**************************** 这是一款生成马赛克拼贴的程序! ****************************" + '\n' +
                    "* 3.生成镶元索引: \n    [颜色众数]: 将选择该图片所有像素点中出现次数最多的BGR值作为该镶元的索引" + '\n' +
                    "    [颜色平均值]: 将选择该图片所有像素点BGR值的平均值作为该镶元的索引" + '\n' +
                    "* 6.绘制图片模式:  \n    [最接近]: 将索引与像素点最接近的镶元用于绘制图片, 此法耗时较长, 且"
                    "可能同一镶元过于聚集的现象, 更适用于镶元较少的场合" + '\n' +
                    "    [取阈值]: 随机选取索引与像素点的欧氏距离小于阈值的镶元用于绘制图片, 若"
                    "不存在, 则将索引与像素点最接近的镶元用于绘制图片, 更适用于镶元较多的场合" + '\n' +
                    "* 生成图片: \n    生成图片的大小为(镶元大小宽 * 调整后的马赛克原图宽) * (镶元大小高 * 调整后的马赛克原图高)" + '\n' +
                    "* 7.颜色修改: \n    生成图片后, 若对生成结果颜色值不满意, 可调整每个镶元的颜色值, 使大图看上去更像原图" + '\n' +
                    "************* 请务必按程序自上而下的顺序进行操作, 否则可能生成失败! *************"
            ), u"使用帮助", wx.ICON_NONE)
            if dlg.ShowModal() == wx.ID_YES:
                self.Close(True)
            dlg.Destroy()

    @staticmethod
    def on_radio_click(event):
        # 事件对象中取出事件源对象(单选按钮)
        rb = event.GetEventObject()
        if rb.GetId() == 30:
            globalVariables.pictureIndexMode = True
            print("设置镶元索引模式 True为众数")
        if rb.GetId() == 31:
            globalVariables.pictureIndexMode = False
            print("设置镶元索引模式 False为平均值")
        if rb.GetId() == 60:
            globalVariables.drawPictureMode = True
            print("绘制图片模式 True为最接近")
        if rb.GetId() == 61:
            globalVariables.drawPictureMode = False
            print("绘制图片模式 False为取阈值")

    def OnEnter(self, evt):  # 判定用户输入的数据是否有效
        try:
            flag = False
            flag = (type(eval(self.tc2_1.GetValue())) == int and int(self.tc2_1.GetValue()) > 0 and
                    type(eval(self.tc2_2.GetValue())) == int and int(self.tc2_2.GetValue()) > 0 and
                    type(eval(self.tc5_1.GetValue())) == int and int(self.tc5_1.GetValue()) > 0 and
                    type(eval(self.tc5_2.GetValue())) == int and int(self.tc5_2.GetValue()) > 0 and
                    type(eval(self.tc6_1.GetValue())) == int and int(self.tc6_1.GetValue()) > 0 and
                    type(eval(self.tc7_1.GetValue())) == int and 100 > int(self.tc7_1.GetValue()) > 0 and
                    int(self.tc2_1.GetValue()) * int(self.tc5_1.GetValue()) < 32768 and
                    int(self.tc2_2.GetValue()) * int(self.tc5_2.GetValue()) < 32768)
        except:
            pass
        if flag:
            globalVariables.resizeX = int(self.tc2_1.GetValue())
            globalVariables.resizeY = int(self.tc2_2.GetValue())
            globalVariables.pictureX = int(self.tc5_1.GetValue())
            globalVariables.pictureY = int(self.tc5_2.GetValue())
            globalVariables.thresholdValue = int(self.tc6_1.GetValue())
            globalVariables.percentage = int(self.tc7_1.GetValue())
            self.prompt.SetLabelText("生成图片大小: (" + str(globalVariables.resizeX) + "*"
                                     + str(globalVariables.pictureX) + ")*("
                                     + str(globalVariables.resizeY) + "*"
                                     + str(globalVariables.pictureY) + ")="
                                     + str(globalVariables.resizeX * globalVariables.pictureX) + "*"
                                     + str(globalVariables.resizeY * globalVariables.pictureY) + "(像素)")
            self.b2.Enable(True)
            self.b3.Enable(True)
            self.b.Enable(True)
        else:
            self.prompt.SetLabelText("错误: 输入错误的参数或生成图片过大! ")
            self.b2.Enable(False)
            self.b3.Enable(False)
            self.b.Enable(False)


def Error(self, e):
    dlg = wx.MessageDialog(None, "发生错误: 请检查操作是否有误! \n" + "错误信息: " + e, u"错误", wx.ICON_ERROR)
    if dlg.ShowModal() == wx.ID_YES:
        self.Close(True)
    dlg.Destroy()


def INFORMATION(self, prompt):
    dlg = wx.MessageDialog(None, "执行完成: " + prompt, u"提示", wx.ICON_INFORMATION)
    if dlg.ShowModal() == wx.ID_YES:
        self.Close(True)
    dlg.Destroy()


def Gauge(self, present, total):
    globalVariables.process = int(100 * (present / total))
    self.gauge.SetValue(globalVariables.process)
    self.textEnd.SetLabelText(str(present) + "/" + str(total))


if __name__ == '__main__':
    app = wx.App()  # 创建应用程序对象
    frm = MyFrame()  # 创建窗口对象
    frm.Show()  # 显示窗口
    app.MainLoop()  # M进入主事件循环
