import os
import cv2
import numpy as np
import wx


filesFilter = ("JPEG文件 (*.jpg; *.jpeg *.jpe)|*.jpg;*.jpeg;*.jpe|"
               "便携式网络图片 (*.png)|*.png|"
               "Windows位图文件 (*.bmp; *.dip)|*.bmp;*.dip|"
               "All files (*.*)|*.*")


def SelectFolder(self, event):  # 选择文件夹
    dlg = wx.DirDialog(self, u"选择镶元图片文件夹", style=wx.DD_DEFAULT_STYLE)
    if dlg.ShowModal() == wx.ID_OK:
        return dlg.GetPath()    # 返回文件夹路径
    dlg.Destroy()


def OpenSingleFile(self, event):
    dlg = wx.FileDialog(self, message="选择马赛克原图", wildcard=filesFilter, style=wx.FD_OPEN)
    if dlg.ShowModal() == wx.ID_OK:
        return dlg.GetPath()    # 返回文件夹路径
    dlg.Destroy()


def SaveFile(self, event):
    dlg = wx.FileDialog(self, message="保存文件...", wildcard=filesFilter, style=wx.FD_SAVE)
    if dlg.ShowModal() == wx.ID_OK:
        return dlg.GetPath()    # 返回文件夹路径
    dlg.Destroy()


def del_file(path_data):
    for i in os.listdir(path_data):  # os.listdir(path_data)#返回一个列表，里面是当前目录下面的所有东西的相对路径
        file_data = path_data + "\\" + i  # 当前文件夹的下面的所有东西的绝对路径
        if os.path.isfile(file_data) is True:  # os.path.isfile判断是否为文件,如果是文件,就删除.如果是文件夹.递归给del_file.
            os.remove(file_data)
        else:
            del_file(file_data)


def myImRead(filename, mode=1):
    raw_data = np.fromfile(filename, dtype=np.uint8)  # 先用numpy把图片文件存入内存：raw_data，把图片数据看做是纯字节数据
    img = cv2.imdecode(raw_data, mode)  # 从内存数据读入图片
    return img


def myImWrite(filename, img):
    cv2.imencode(filename[-4:], img)[1].tofile(filename)